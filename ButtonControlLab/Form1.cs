﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ch01_Button
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.uctlButton1.Click += UctlButton1_Click;
            this.uctlButton1.UserControlClick += UctlButton1_UserControlClick;
        }

        private void UctlButton1_Click(object sender, EventArgs e)
        {
            string message = string.Format("標準點擊按鈕:點擊類型[{0}]", sender.GetType().ToString());
            MessageBox.Show(message);
        }

        private void UctlButton1_UserControlClick(object sender, EventArgs e)
        {
            string message = string.Format("使用者點擊按鈕:點擊類型[{0}]", sender.GetType().ToString());
            MessageBox.Show(message);
        }
    }
}
