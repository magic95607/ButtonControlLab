﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ch01_Button.DefineButton;

namespace ch01_Button
{
    public partial class uctlButton : UserControl
    {
        public event EventHandler UserControlClick;
        /// <summary>
        /// 加入Button功能
        /// </summary>
        public CustomButton _ButtonBasic { set; get; }
        public uctlButton()
        {
            InitializeComponent();
        }

        public new event EventHandler Click
        {
            add
            {
                base.Click += value;
                _ButtonBasic.ControlsAddClick(this, value);
            }
            remove
            {
                base.Click -= value;
                _ButtonBasic.ControlsRemoveClick(this, value);
            }
        }

        private void uctlButton_Load(object sender, EventArgs e)
        {
            _ButtonBasic = new CustomButton();
            this.Click += UctlButton_Click;
        }

        private void UctlButton_Click(object sender, EventArgs e)
        {
            var current = _ButtonBasic.GetUserControl(sender as Control);
            UserControlClick(current, e);
        }
    }
}
