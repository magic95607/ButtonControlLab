﻿namespace ch01_Button
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            ch01_Button.DefineButton.CustomButton buttonBasic_11 = new ch01_Button.DefineButton.CustomButton();
            this.uctlButton1 = new ch01_Button.uctlButton();
            this.SuspendLayout();
            // 
            // uctlButton1
            // 
            this.uctlButton1._ButtonBasic = buttonBasic_11;
            this.uctlButton1.Location = new System.Drawing.Point(12, 12);
            this.uctlButton1.Name = "uctlButton1";
            this.uctlButton1.Size = new System.Drawing.Size(340, 155);
            this.uctlButton1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 178);
            this.Controls.Add(this.uctlButton1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private uctlButton uctlButton1;
    }
}

