﻿namespace ch01_Button
{
    partial class uctlButton
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTag = new System.Windows.Forms.Label();
            this.btnButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTag
            // 
            this.lbTag.BackColor = System.Drawing.Color.DarkGray;
            this.lbTag.Location = new System.Drawing.Point(25, 24);
            this.lbTag.Name = "lbTag";
            this.lbTag.Size = new System.Drawing.Size(133, 51);
            this.lbTag.TabIndex = 0;
            this.lbTag.Text = "標籤類型";
            this.lbTag.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnButton
            // 
            this.btnButton.Location = new System.Drawing.Point(25, 78);
            this.btnButton.Name = "btnButton";
            this.btnButton.Size = new System.Drawing.Size(133, 51);
            this.btnButton.TabIndex = 3;
            this.btnButton.Text = "按鈕類型";
            this.btnButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(164, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 116);
            this.panel1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 51);
            this.button1.TabIndex = 6;
            this.button1.Text = "按鈕類型";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGray;
            this.label1.Location = new System.Drawing.Point(10, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 51);
            this.label1.TabIndex = 5;
            this.label1.Text = "標籤類型";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uctlButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnButton);
            this.Controls.Add(this.lbTag);
            this.Name = "uctlButton";
            this.Size = new System.Drawing.Size(340, 155);
            this.Load += new System.EventHandler(this.uctlButton_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbTag;
        private System.Windows.Forms.Button btnButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
    }
}
