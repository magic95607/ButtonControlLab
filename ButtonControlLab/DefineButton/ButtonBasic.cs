﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ch01_Button.DefineButton
{
    public class ButtonBasic<TUserControl> where TUserControl : UserControl
    {
        /// <summary>
        /// 為底層控制移除Click
        /// </summary>
        /// <param name="pa_UserControl"></param>
        /// <param name="pa_EventHandler"></param>
        public void ControlsRemoveClick(Control pa_UserControl, EventHandler pa_EventHandler)
        {
            if (pa_UserControl.Controls.Count == 0)
                return;
            foreach (Control Control in pa_UserControl.Controls)
            {
                Control.Click -= pa_EventHandler;
                ControlsRemoveClick(Control, pa_EventHandler);
            }
        }

        /// <summary>
        /// 為底層控制加上Click
        /// </summary>
        /// <param name="pa_UserControl"></param>
        /// <param name="pa_EventHandler"></param>
        public void ControlsAddClick(Control pa_UserControl, EventHandler pa_EventHandler)
        {
            if (pa_UserControl.Controls.Count == 0)
                return;
            foreach (Control Control in pa_UserControl.Controls)
            {
                Control.Click += pa_EventHandler;
                ControlsAddClick(Control, pa_EventHandler);
            }
        }

        /// <summary>
        /// 找到當前UserControl外層物件
        /// </summary>
        /// <param name="pa_Control"></param>
        /// <returns></returns>
        public TUserControl GetUserControl(Control pa_Control)
        {
            if (pa_Control is TUserControl)
            {
                return pa_Control as TUserControl;
            }
            else
            {
                if (pa_Control.Parent is TUserControl)
                {
                    return pa_Control.Parent as TUserControl;
                }
                else
                {
                    return GetUserControl(pa_Control.Parent);
                }
            }
        }
    }
}
